-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2021 at 03:04 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bomtanhd`
--

-- --------------------------------------------------------

--
-- Table structure for table `actor`
--

CREATE TABLE `actor` (
  `id` int(11) NOT NULL,
  `ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nghiepvu` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gioithieu` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `actor`
--

INSERT INTO `actor` (`id`, `ten`, `nghiepvu`, `avatar`, `gioithieu`, `slug`) VALUES
(1, 'Từ Hảo', 'dienvien', 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Xuhao1.jpg/250px-Xuhao1.jpg', 'Từ Hảo (sinh ngày 30 tháng 10 năm 1995) tại Diêm Thành, Giang Tô,  Trung Quốc, là nữ diễn viên người Trung Quốc, tốt nghiệp Học viện Điện ảnh Bắc Kinh. Cô được biết đến với vai Ôn Tiểu Noãn trong phim Bạn gái lầu dưới xin ký nhận, Hạo Lan truyện vai Vân Mộng Công chúa, Huyền Môn Đại Sư vai Tử Lưu Ly, Người Thừa Kế vai Tiểu Tịnh và Run Rẩy Đi A Bộ vai Tiêu Như Ý.', 'tu-hao'),
(2, 'Vương Tinh', 'daodien', 'https://static1.dienanh.net/upload/2014/05/12/vuong-tinh-134.jpg', 'Vương Tinh (chữ Hán: 王晶\\\\\\\\\\\\\\\\; bính âm: Wáng Jīng\\\\\\\\\\\\\\\\; tên tiếng Anh: Wong Jing\\\\\\\\\\\\\\\\; sinh ngày 3 tháng 5 năm 1955) là một nhà làm phim của điện ảnh Hồng Kông. Trong vai trò nhà sản xuất', 'vuong-tinh'),
(3, 'Shigeru Chiba', 'daodien', 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/9oajq1i3qzkA2MTDH3ciLOEenS9.jpg', 'Masaharu Maeda (前田 正治, Maeda Masaharu), known by the stage name Shigeru Chiba (千葉 繁, Chiba Shigeru), is a Japanese actor and voice actor. He has also worked as a sound effects director and music director. He is affiliated with the talent management firm 81 Produce.  He is most known for the roles of the narrator of Fist of the North Star, Megane (Urusei Yatsura), Rei Ichidō (High School\\! Kimengumi), Kazuma Kuwabara (Yu Yu Hakusho), Pilaf (Dragon Ball), Raditz and Garlic Jr. (Dragon Ball Z), Buggy the Clown (One Piece) Kefka Palazzo (Dissidia: Final Fantasy) and Kōichi Todome (Kerberos saga).', 'shigeru-chiba'),
(4, 'Thành Long', 'dienvien', 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/nraZoTzwJQPHspAVsKfgl3RXKKa.jpg', 'Jackie Chan (Chinese: 成龍\\; born 7 April 1954), born Chan Kong-sang, is a Hong Kong actor, action choreographer, filmmaker, comedian, producer, martial artist, screenwriter, entrepreneur, singer and stunt performer. In his movies, he is known for his acrobatic fighting style, comic timing, use of improvised weapons, and innovative stunts. Jackie Chan has been acting since the 1970s and has appeared in over 100 films.  Chan has received stars on the Hong Kong Avenue of Stars and the Hollywood Walk of Fame. As a cultural icon, Chan has been referenced in various pop songs, cartoons, and video games. Chan is also a Cantopop and Mandopop star, having released a number of albums and sung many of the theme songs for the films in which he has starred.', 'thanh-long'),
(5, 'Asami Seto', 'dienvien', 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/j88IskzIdGkyHBbjygPyg9EiE90.jpg', 'Asami Seto is a Japanese voice actress affiliated with Sigma Seven e. She made her debut in the television anime Hourou Musuko.', 'asami-seto'),
(6, 'Junya Enoki', 'dienvien', 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/vBnNL3Jqy0zkS3ZgsXZmvDM9Dfz.jpg', 'We don&apos\\;t have a biography for Junya Enoki.', 'junya-enoki');

-- --------------------------------------------------------

--
-- Table structure for table `binhluan`
--

CREATE TABLE `binhluan` (
  `id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thich` int(100) NOT NULL,
  `thoiGian` timestamp NOT NULL DEFAULT current_timestamp(),
  `traloi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `binhluan`
--

INSERT INTO `binhluan` (`id`, `noidung`, `username`, `thich`, `thoiGian`, `traloi`) VALUES
('14012021070804vuongnguyen', 'hello', 'vuongnguyen', 11, '2021-01-14 08:48:57', ',14012021081027hieugiddy,14012021090604hieugiddy,14012021094857hieugiddy'),
('14012021072157hieugiddy', 'hay', 'hieugiddy', 16, '2021-01-14 08:10:27', ',14012021090749hieugiddy,14012021091027hieugiddy,14012021091036hieugiddy'),
('14012021090604hieugiddy', 'tuyệt', 'hieugiddy', 4, '2021-01-14 08:06:04', NULL),
('14012021090749hieugiddy', 'được', 'hieugiddy', 3, '2021-01-14 08:07:49', NULL),
('14012021091027hieugiddy', 'phim hay lắm', 'hieugiddy', 3, '2021-01-14 08:10:27', NULL),
('14012021091036hieugiddy', 'xin chào', 'hieugiddy', 4, '2021-01-14 08:10:36', NULL),
('14012021092754hieugiddy', 'sfd', 'hieugiddy', 0, '2021-01-14 08:27:54', NULL),
('14012021092813hieugiddy', 'a\r\n', 'hieugiddy', 0, '2021-01-14 08:28:13', NULL),
('14012021092906hieugiddy', 'zsdf', 'hieugiddy', 0, '2021-01-14 08:29:06', NULL),
('14012021093004hieugiddy', 'zdf', 'hieugiddy', 0, '2021-01-14 08:30:04', NULL),
('14012021093925hieugiddy', 'hay', 'hieugiddy', 0, '2021-01-14 08:39:25', NULL),
('14012021094857hieugiddy', 'ok', 'hieugiddy', 1, '2021-01-14 08:48:57', NULL),
('14012021102113nguyenvanquocvuong', 'phim dơẻ', 'nguyenvanquocvuong', 5, '2021-01-14 09:55:01', ',14012021102128nguyenvanquocvuong,14012021105501hoaha'),
('14012021102128nguyenvanquocvuong', 'ko biết xem phim rồi ', 'nguyenvanquocvuong', 6, '2021-01-14 09:21:28', NULL),
('14012021102753hieugiddy', 'được', 'hieugiddy', 0, '2021-01-14 09:27:53', NULL),
('14012021105402hoaha', 'hay', 'hoaha', 2, '2021-01-14 09:54:02', NULL),
('14012021105501hoaha', 'phim hay mà', 'hoaha', 2, '2021-01-14 09:55:01', NULL),
('15012021144940nguyenvanquocvuong', 'hay\r\n', 'nguyenvanquocvuong', 10, '2021-01-15 13:49:53', ',15012021144953nguyenvanquocvuong'),
('15012021144953nguyenvanquocvuong', 'phim rất hay\r\n', 'nguyenvanquocvuong', 5, '2021-01-15 13:49:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chitietbinhluan`
--

CREATE TABLE `chitietbinhluan` (
  `idBinhLuan` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idPhim` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chitietbinhluan`
--

INSERT INTO `chitietbinhluan` (`idBinhLuan`, `idPhim`) VALUES
('14012021070804vuongnguyen', 253),
('14012021072157hieugiddy', 253),
('14012021090604hieugiddy', 261),
('14012021102113nguyenvanquocvuong', 253),
('14012021102753hieugiddy', 254),
('14012021105402hoaha', 259),
('15012021144940nguyenvanquocvuong', 259);

-- --------------------------------------------------------

--
-- Table structure for table `chitietdaodien`
--

CREATE TABLE `chitietdaodien` (
  `idPhim` int(11) NOT NULL,
  `idDaoDien` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chitietdaodien`
--

INSERT INTO `chitietdaodien` (`idPhim`, `idDaoDien`) VALUES
(249, 'vuong-tinh'),
(251, 'vuong-tinh'),
(0, 'vuong-tinh'),
(253, 'vuong-tinh'),
(254, 'vuong-tinh'),
(264, 'shigeru-chiba'),
(263, 'shigeru-chiba'),
(256, 'vuong-tinh'),
(255, 'vuong-tinh');

-- --------------------------------------------------------

--
-- Table structure for table `chitietdienvien`
--

CREATE TABLE `chitietdienvien` (
  `idPhim` int(11) NOT NULL,
  `idDienVien` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chitietdienvien`
--

INSERT INTO `chitietdienvien` (`idPhim`, `idDienVien`) VALUES
(253, 'tu-hao'),
(253, 'thanh-long'),
(254, 'thanh-long'),
(264, 'asami-seto'),
(263, 'asami-seto'),
(263, 'junya-enoki'),
(261, 'thanh-long'),
(259, 'tu-hao'),
(260, 'thanh-long'),
(256, 'tu-hao'),
(262, 'thanh-long'),
(255, 'tu-hao');

-- --------------------------------------------------------

--
-- Table structure for table `chitietphim`
--

CREATE TABLE `chitietphim` (
  `id` int(8) NOT NULL,
  `gioithieu` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `vote` float DEFAULT NULL,
  `poster` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imdb` float NOT NULL,
  `trailer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tagline` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `anhbia` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tukhoa` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chitietphim`
--

INSERT INTO `chitietphim` (`id`, `gioithieu`, `vote`, `poster`, `status`, `imdb`, `trailer`, `tagline`, `anhbia`, `tukhoa`) VALUES
(260, 'Bộ phim kể câu chuyện về Park Hoon (Lee Jong Suk), một cậu bé bị bắt cóc đến Triều Tiên cùng người cha bác sĩ. Ở Triều Tiên, Park Hoon được người cha của mình đào tạo thành bác sĩ. Park Hoon trở thành thiên tài phẫu thuật lồng ngực. Anh tìm cách trở về lại Hàn Quốc. Park Hoon được nhận vào làm tại bệnh viện danh tiếng nhất Hàn Quốc, bệnh viện Đại học Dong Woo nhưng anh lại cảm thấy mình như người ngoài. Để mang vể tình yêu của mình ở Bắc Hàn, anh làm bất cứ điều gì có thể để kiếm tiền...', 5, 'https://image.tmdb.org/t/p/w300//kRkhyJCfGvaQjGvOVupH9n4k02g.jpg', '20/20', 8, 'https://www.youtube.com/embed/0-xEHRngUNQ', 'Tagline', 'https://www.fullphim.net/static/5fe2d564b3fa6403ffa11d1c/5fe2d564b3fa6479b0a12723_banner-bac-si-xu-la.jpg', '닥터 이방인 (2014),Bác Sĩ Xứ Lạ -  Doctor Stranger (2014) - 닥터 이방인 (2014) | BomTanHD,'),
(261, 'Hé lộ khâu chuẩn bị cho các buổi biểu diễn, bộ phim tài liệu giới thiệu về thần tượng nhạc pop Việt Nam Sơn Tùng M-TP và đam mê ẩn sau chuyến lưu diễn Sky Tour của anh.', 4, 'https://image.tmdb.org/t/p/w300//dFyT5KUkcoa77mrX5PvlZuWuRBS.jpg', '94 phút', 0, 'https://www.youtube.com/embed/t7m1iqs_b-U', '', 'https://media.travelmag.vn/files/news/2020/06/15/sieu-pham-dien-anh-cua-son-tung-m-tp-sky-tour-movie-co-dang-xem-hay-khong-174124.jpg', 'Sky Tour: The Movie (2020),Sơn Tùng M-TP: Sky Tour Movie (2020) - Sky Tour: The Movie (2020) | BomTanHD,'),
(253, 'CEO của công ty An Ninh Văn Hóa - Diệp Phi Mặc bỗng dưng bị bệnh mà biến mất khỏi tầm mắt khán giả khiến mọi người đoán An Ninh Văn Hóa đang gặp khủng hoảng. Mẹ Diệp cho rằng con trai vì nhớ mãi không quên bạn gái cũ mà sinh tâm bệnh nên đã thuê Ôn Tiểu Noãn - nữ diễn viên quần chúng giả làm người yêu thầm Diệp Phi Mặc. Vì kiếm tiền để duy trì gánh hát Hoàng Mai ở quê nhà mà Ôn Tiểu Noãn chấp nhận phi vụ này. Nhưng qua tiếp xúc hai người &quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;phim giả tình thật&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\; trở thành couple thực sự.', 5, 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/bQmRNBMXiFCu2nx3v0Q4Y6m4dqF.jpg', '36/36', 9.9, 'https://www.youtube.com/embed/07nYT0SP-FE', 'Bạn Gái Lầu Dưới Xin Hãy Ký Nhận ', '/bomtanhd/public/img/upload/140120211456501.jpg', '楼下女友请签收 (2020),Bạn Gái Lầu Dưới Xin Hãy Ký Nhận - Girlfriend (2020) - 楼下女友请签收 (2020) | BomTanHD,'),
(259, 'Sự kết hợp chưa từng có giữa Thanh Hằng và Chi Pu cùng câu chuyện ly kỳ về yêu - hận - tình - thù đằng sau hai người phụ nữ xinh đẹp chắc chắn sẽ khiến bạn tò mò.', 3, 'https://image.tmdb.org/t/p/w300//2bAjCwJ1sZMtpr1GywZSboKqXW1.jpg', '104 phút', 9, 'https://www.youtube.com/embed/sPny1CS62bY', 'Tất cả chỉ là dối trá thôi em à', 'https://topreview.vn/wp-content/uploads/2020/01/chi-chi-em-em.jpg', 'Chị Chị Em Em (2019),Chị Chị Em Em (2019) - Chị Chị Em Em (2019) | BomTanHD,Tất cả chỉ là dối trá thôi em à'),
(254, 'Người Tiên Phong bộ phim điện ảnh kể về công ty bảo an quốc tế Cấp Tiên Phong trong quá trình bảo vệ thương nhân Thái Quốc Lập và con gái ông là Fareeda đã anh dũng đối đầu với ” Sói Bắc Cực ” và đập tan kế hoạch do tổ chức sau lưng “Sói Bắc Cực” vạch ra.', 3, 'https://image.tmdb.org/t/p/w300//quqEuH1fhC3SbhJx00hN5lDNaEF.jpg', '107 phút', 7, 'https://www.youtube.com/embed/sIr7k_gkaqU', '', '/bomtanhd/public/img/upload/140120211502161.jpg', '急先锋 (2020),Người Tiên Phong (2020) - 急先锋 (2020) | BomTanHD,'),
(255, 'Quyết định đâm đầu vào chỗ chết, cựu quân nhân Jung Seok trở lại bán đảo vì số tiền hậu hĩnh lên đến 2,5 triệu USD. Tuy nhiên, anh và đồng đội bất ngờ bị phục kích bởi lữ đoàn biến chất 631 cùng bầy đàn zombie khát máu. Được gia đình Min Jung giải cứu, Jung Seok phải tìm cách đào thoát khỏi nơi đây trước khi quá trễ - Luật lệ duy nhất là sống sót', 1, 'https://image.tmdb.org/t/p/w300//9hLmHGY1qIdCI5fJ4JvAXWQQORk.jpg', '114 phút', 6, 'https://www.youtube.com/embed/J8riYadR3Nk', 'Luật lệ duy nhất là sống sót.', 'https://photo2.tinhte.vn/data/attachment-files/2020/08/5100226_Train_to_Busan_2_Vietnam.jpg', '반도 (2020),Bán Đảo Peninsula (2020) - 반도 (2020) | BomTanHD,Luật lệ duy nhất là sống sót.'),
(256, 'Một tác phẩm đến từ hãng phim kinh dị Blumhouse và được nhào nặn qua nhà sản xuất của siêu phẩm INVISIBLE MAN và SPLIT, phim xoay quanh 4 cô gái phù thủy đang dần khai phá những tiềm năng ma thuật mới.', 5, 'https://image.tmdb.org/t/p/w300//yAktZ8CpwBkuIyUyUtKf6VFvXAI.jpg', '94 phút', 6, 'https://www.youtube.com/embed/P5Zg6rQLJGI', '', 'https://touchcinema.com/storage/slider-app/phuthuyhocuodng.jpg', 'The Craft: Legacy (2020),Phù Thủy Học Đường (2020) - The Craft: Legacy (2020) | BomTanHD,'),
(262, '', 4.4, 'https://1.bp.blogspot.com/-RpWTEZm7eBw/Xp-epQJlOqI/AAAAAAAAIf0/glzdthFzkK4bmax5ljOBu3jWpjECbKiaQCK4BGAsYHg/Seal%2BTeam%2B2%2B-%2BCover%2BVne%2BSeal%2B%2BTeam%2BPh%25E1%25BA%25A7n%2B2.jpg', '69/69', 8, 'https://www.youtube.com/embed/pGpTias55hA', '', 'https://i.pinimg.com/originals/ec/ee/0e/ecee0ecd48a9924d83ae248bfa1bb86a.jpg', 'SEAL Team (2017),SEAL Team (2017) - SEAL Team (2017) | BomTanHD,'),
(263, 'Vì một lý do kỳ lạ nào đó, Yuji Itadori, mặc dù với thể chất hoàn hảo nhưng anh lại đâm đầu vào tham gia CLB Huyền Bí. Tuy nhiên, họ đã sớm phát hiện ra là những câu chuyện huyền bí hoàn toàn có thật khi các thành viên trong CLB lần lượt bị tấn công\\\\\\\\\\\\! Trong khi đó, Megumi Fushiguro “bí ẩn” lại đang truy tìm một đối tượng bị nguyền rủa cấp đặc biệt và cuộc tìm kiếm này đã đưa nhóm bạn đến Itadori', 0, 'https://image.tmdb.org/t/p/w300//9MSNijZyyUGoRv01aUKkEYxccWB.jpg', '24/24', 8.7, 'https://www.youtube.com/embed/MltDhYeBXb4', 'A boy fights... for &quot\\\\\\\\\\\\;the right death.&quot\\\\\\\\\\\\;', 'https://anivsub.org/wp-content/uploads/2020/09/Jujutsu-Kaisen.jpg', '呪術廻戦 (2020),Vật Thể Bị Nguyền Rủa - Sorcery Fight (2020) - 呪術廻戦 (2020) | BomTanHD,A boy fights... for &quot\\\\\\\\\\\\;the right death.&quot\\\\\\\\\\\\;'),
(264, '<span style=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\\\;color:&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; rgb(205,=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; 205,=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; 205)\\\\\\\\\\\\\\\\\\\\\\;=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; font-family:=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; &quot\\\\\\\\\\\\\\\\\\\\\\;roboto=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; condensed&quot\\\\\\\\\\\\\\\\\\\\\\;,=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; sans-serif\\\\\\\\\\\\\\\\\\\\\\;=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; font-size:=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; 16px\\\\\\\\\\\\\\\\\\\\\\;=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; text-align:=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; justify\\\\\\\\\\\\\\\\\\\\\\;=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; background-color:=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; rgba(0,=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; 0,=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\; 0.3)\\\\\\\\\\\\\\\\\\\\\\;&quot\\\\\\\\\\\\\\\\\\\\\\;=&quot\\;&quot\\;\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;\\\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;\\\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;\\\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;\\\\\\\\\\;&quot\\;\\\\;\\\\\\;\\\\\\\\;&quot\\;\\\\;\\\\\\;&quot\\;\\\\;&quot\\;>Mulan (2020) được chuyển thể từ phim hoạt hình cùng tên của Disney ra mắt năm 1998. Bộ phim kể về Mộc Lan – một cô gái hiếu thảo đã giả trai, thay cha tuổi già sức yếu tòng quân ra trận.</span>', 0, 'https://lumiere-a.akamaihd.net/v1/images/p_mulan2020_20204_b005decc.jpeg', 'Đã phát hành', 10, 'https://www.youtube.com/embed/DN8oebcXcMc', 'Mulan', 'https://bloganchoi.com/wp-content/uploads/2020/02/mulan-2020-cam-chieu-7.jpg', 'Hoa Mộc Lan (2020),Mulan (2020),Luu Diec Phi,');

-- --------------------------------------------------------

--
-- Table structure for table `chitietquocgia`
--

CREATE TABLE `chitietquocgia` (
  `idPhim` int(11) NOT NULL,
  `idQuocGia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chitietquocgia`
--

INSERT INTO `chitietquocgia` (`idPhim`, `idQuocGia`) VALUES
(253, 'trung-quoc'),
(254, 'trung-quoc'),
(254, 'hong-kong'),
(264, 'trung-quoc'),
(264, 'my'),
(263, 'nhat-ban'),
(261, 'viet-nam'),
(259, 'viet-nam'),
(260, 'han-quoc'),
(256, 'my'),
(262, 'my'),
(255, 'han-quoc');

-- --------------------------------------------------------

--
-- Table structure for table `chitiettheloai`
--

CREATE TABLE `chitiettheloai` (
  `idPhim` int(11) NOT NULL,
  `idTheLoai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chitiettheloai`
--

INSERT INTO `chitiettheloai` (`idPhim`, `idTheLoai`) VALUES
(253, 'ngon-tinh'),
(254, 'vo-thuat'),
(254, 'hanh-dong'),
(254, 'hinh-su'),
(264, 'tien-hiep'),
(264, 'vo-thuat'),
(263, 'hai-huoc'),
(263, 'anime'),
(261, 'hai-huoc'),
(261, 'tam-ly'),
(259, 'ngon-tinh'),
(259, 'hai-huoc'),
(259, 'tam-ly'),
(260, 'ngon-tinh'),
(260, 'tam-ly'),
(256, 'hai-huoc'),
(256, 'kinh-di'),
(256, 'tam-ly'),
(262, 'hanh-dong'),
(262, 'hinh-su'),
(255, 'ngon-tinh'),
(255, 'hanh-dong'),
(255, 'tam-ly');

-- --------------------------------------------------------

--
-- Table structure for table `danhsachphat`
--

CREATE TABLE `danhsachphat` (
  `id` int(11) NOT NULL,
  `ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dsphim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `tieude` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tenweb` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mota` text COLLATE utf8_unicode_ci NOT NULL,
  `tukhoa` text COLLATE utf8_unicode_ci NOT NULL,
  `favicon` text COLLATE utf8_unicode_ci NOT NULL,
  `logo` text COLLATE utf8_unicode_ci NOT NULL,
  `luottruycap` int(11) NOT NULL,
  `linkweb` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tacgia` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`tieude`, `tenweb`, `mota`, `tukhoa`, `favicon`, `logo`, `luottruycap`, `linkweb`, `tacgia`) VALUES
('BomTanHD - Film Bom Tấn Hành Động HD', 'BomTanHD CC', 'Tuyển tập phim hay', 'bomtanhd,phim hay, phim chieu rap moi, vuong bro, hehe', '/bomtanhd/public/img/upload/14012021104401favicon.ico', '/bomtanhd/public/img/upload/14012021104424bomtanhd.png', 20, '/bomtanhd', 'Nguyễn Văn Quốc Vương, Nguyễn Trần Linh Vương, Lê Đăng QUỳnh');

-- --------------------------------------------------------

--
-- Table structure for table `linkphim`
--

CREATE TABLE `linkphim` (
  `id` int(11) NOT NULL,
  `idPhim` text COLLATE utf8_unicode_ci NOT NULL,
  `tenhienthi` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL,
  `loai` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `server` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ngaythem` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `linkphim`
--

INSERT INTO `linkphim` (`id`, `idPhim`, `tenhienthi`, `link`, `loai`, `server`, `ngaythem`) VALUES
(4, 'ban-gai-lau-duoi-xin-hay-ky-nhan---girlfriend-2020---2020-bomtanhd--2020', 'Tập 1', 'https://drive.google.com/file/d/1p5RVdW1XRY8ManmiO5RYv2VgZHYdQ7BN/preview', 'xem', 'Siêu đỉnh', '2021-01-10 12:27:02'),
(5, 'ban-gai-lau-duoi-xin-hay-ky-nhan---girlfriend-2020---2020-bomtanhd--2020', 'Tập 2', 'https://drive.google.com/file/d/1BrZX8yhBVaui-TXjmAjJ-_FTNmIhZUQ0/preview', 'xem', 'Siêu nhanh', '2021-01-10 12:27:07'),
(6, 'ban-gai-lau-duoi-xin-hay-ky-nhan---girlfriend-2020---2020-bomtanhd--2020', 'Tập 3', 'https://drive.google.com/file/d/1p5RVdW1XRY8ManmiO5RYv2VgZHYdQ7BN/preview', 'xem', 'Siêu nhanh', '2021-01-10 12:27:11'),
(7, 'ban-gai-lau-duoi-xin-hay-ky-nhan---girlfriend-2020---2020-bomtanhd--2020', 'Tập 4', 'https://drive.google.com/file/d/1p5RVdW1XRY8ManmiO5RYv2VgZHYdQ7BN/preview', 'xem', 'Siêu nhanh', '2021-01-10 12:27:16'),
(8, 'ban-gai-lau-duoi-xin-hay-ky-nhan---girlfriend-2020---2020-bomtanhd--2020', 'Tập 1', 'https://drive.google.com/file/d/1BrZX8yhBVaui-TXjmAjJ-_FTNmIhZUQ0/preview', 'xem', 'Siêu vip', '2021-01-10 12:27:21'),
(16, 'ban-gai-lau-duoi-xin-hay-ky-nhan---girlfriend-2020---2020-bomtanhd--2020', 'Hiếu', 'https://drive.google.com/file/d/1p5RVdW1XRY8ManmiO5RYv2VgZHYdQ7BN/preview', 'xem', 'Siêu đỉnh', '2021-01-14 09:29:52'),
(10, 'ban-gai-lau-duoi-xin-hay-ky-nhan---girlfriend-2020---2020-bomtanhd--2020', 'Tập 2', 'https://bomtanhd.net/phim-le/chuyen-vieng-tham-tu-than-2019-guests--gosti-2019.html', 'tai', 'Siêu nhanh', '2021-01-10 12:27:34'),
(11, 'ban-gai-lau-duoi-xin-hay-ky-nhan---girlfriend-2020---2020-bomtanhd--2020', 'Tập 3', 'https://bomtanhd.net/phim-le/chuyen-vieng-tham-tu-than-2019-guests--gosti-2019.html', 'tai', 'Siêu nhanh', '2021-01-10 12:27:38'),
(12, 'ban-gai-lau-duoi-xin-hay-ky-nhan---girlfriend-2020---2020-bomtanhd--2020', 'Tập 4', 'https://bomtanhd.net/phim-le/chuyen-vieng-tham-tu-than-2019-guests--gosti-2019.html', 'tai', 'Siêu nhanh', '2021-01-10 12:27:52'),
(13, 'ban-gai-lau-duoi-xin-hay-ky-nhan---girlfriend-2020---2020-bomtanhd--2020', 'Tập 5', 'https://bomtanhd.net/phim-le/chuyen-vieng-tham-tu-than-2019-guests--gosti-2019.html', 'tai', 'Siêu nhanh', '2021-01-10 12:27:59'),
(36, 'bac-si-xu-la---doctor-stranger-2014---2014-bomtanhd--2014', 'Tập 1', 'https://www.youtube.com/embed/T0MG-OKf59k', 'xem', 'Siêu nhanh', '2021-01-14 12:51:01'),
(17, 'vat-the-bi-nguyen-rua---sorcery-fight-2020---2020-bomtanhd--2020', 'Tập 1', 'https://www.youtube.com/embed/cjBt8nk_KkI', 'xem', 'Siêu nhanh', '2021-01-14 09:38:51'),
(19, 'son-tung-m-tp-sky-tour-movie-2020---sky-tour-the-movie-2020-bomtanhd-sky-tour-the-movie-2020', 'Full hd', 'https://www.youtube.com/embed/_1rXcxynH4U', 'xem', 'Siêu nhanh', '2021-01-14 12:12:10'),
(20, 'chi-chi-em-em-2019---chi-chi-em-em-2019-bomtanhd-chi-chi-em-em-2019', 'Full hd', 'https://www.youtube.com/embed/kfa8YzqOWK8', 'xem', 'Siêu nhanh', '2021-01-14 12:22:13'),
(21, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 1', 'https://www.youtube.com/embed/EVl2jd6FAoM', 'xem', 'Siêu nhanh', '2021-01-14 12:35:11'),
(22, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 2', 'https://www.youtube.com/embed/xTiWQNleIEg', 'xem', 'Siêu nhanh', '2021-01-14 12:35:31'),
(23, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 3', 'https://www.youtube.com/embed/KD9lbQnt81s', 'xem', 'Siêu nhanh', '2021-01-14 12:35:54'),
(24, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 4', 'https://www.youtube.com/embed/Z8P70aI6zCw', 'xem', 'Siêu nhanh', '2021-01-14 12:36:17'),
(25, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 5', 'https://www.youtube.com/embed/1_yZ6ncRXqo', 'xem', 'Siêu nhanh', '2021-01-14 12:37:05'),
(26, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 6', 'https://www.youtube.com/embed/tN1phYtereg', 'xem', 'Siêu nhanh', '2021-01-14 12:37:25'),
(27, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 7', 'https://www.youtube.com/embed/pbGQKYot6BI', 'xem', 'Siêu nhanh', '2021-01-14 12:37:46'),
(35, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 7', 'https://redirector.googlevideo.com/videoplayback?expire=1610649917&ei=3TwAYPD-GcjQvAS_6rroDA&ip=111.94.18.229&id=o-APMuGx6yfXQJXg0f6_8eISMMeUBdgCCJbJ0khbO8tXOW&itag=22&source=youtube&requiressl=yes&mh=TQ&mm=31%2C29&mn=sn-4pgnuhxqp5-jb36%2Csn-4pgnuhxqp5-jb3k&ms=au%2Crdu&mv=m&mvi=1&pl=24&initcwndbps=281250&vprv=1&mime=video%2Fmp4&ns=8kuEOo_l2QkkxrkJYIb3K6cF&ratebypass=yes&dur=1622.912&lmt=1587203604740381&mt=1610628048&fvip=4&c=WEB&txp=5535432&n=WVSp5aYuB7UtUE&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRgIhAOpg2LZ2mZnsxbpAYb3g4AuJs8_393MqN3rkw5_i5DQXAiEA-dQVH4IxCtU9Ae1LlZyehlL4nmrx_U1YR6CvgOWh-gk%3D&sig=AOq0QJ8wRQIhAOpp-WcnBlL3j2HZAu6VL6lIumY1PyFP4Zq_iv0kxLCRAiA248ZvNdV62dHn_KxsBTSHNyuLoZrawTysK390x7KqNQ==&title=THUY%E1%BA%BET+MINH+%7C+Ng%C3%B4n+T%C3%ACnh+Hoa+Ng%E1%BB%AF+2020+Si%C3%AAu+Hay+%7C+B%E1%BA%A1n+G%C3%A1i+L%E1%BA%A7u+D%C6%B0%E1%BB%9Bi+Xin+H%C3%A3y+K%C3%BD+Nh%E1%BA%ADn+-+T%E1%BA%ADp+07', 'tai', 'Siêu nhanh', '2021-01-14 12:39:30'),
(29, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 1', 'https://redirector.googlevideo.com/videoplayback?expire=1610649697&ei=ATwAYIa5OM7K4-EPwoCEaA&ip=36.76.121.240&id=o-AECfsWjxmuksrM6xBLkwtUxncBGMJ_gTNJmsDrZu1ELJ&itag=22&source=youtube&requiressl=yes&mh=LY&mm=31%2C29&mn=sn-2uuxa3vh-n0cs%2Csn-npoe7nl6&ms=au%2Crdu&mv=m&mvi=7&pl=20&initcwndbps=318750&vprv=1&mime=video%2Fmp4&ns=P0N4c49TMkgx_8tkwrn5V4gF&ratebypass=yes&dur=2025.778&lmt=1587285076707374&mt=1610627811&fvip=1&c=WEB&txp=5535432&n=MKnwZMUrXaJp43&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRQIgJc0w15hw_jouOkD0-mr8HOSzHkzLDO6ntbjCj6By4G8CIQCKK6LRoNPCHsy7lAB4ZCSVpBr3b0AeuhfEUwhJcOJasA%3D%3D&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRAIgWPOnyJvVQvczoMLXUq3wZs2l0wH-RMud3sMdJdnZF78CIAORpb9d6G6_0uu1vYA1BmA3Gv7Fk2uyuWGQia8jfoFY&title=THUY%E1%BA%BET+MINH+%7C+Ng%C3%B4n+T%C3%ACnh+Hoa+Ng%E1%BB%AF+2020+Si%C3%AAu+Hay+%7C+B%E1%BA%A1n+G%C3%A1i+L%E1%BA%A7u+D%C6%B0%E1%BB%9Bi+Xin+H%C3%A3y+K%C3%BD+Nh%E1%BA%ADn+-+T%E1%BA%ADp+01', 'tai', 'Siêu nhanh', '2021-01-14 12:38:30'),
(30, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 2', 'https://redirector.googlevideo.com/videoplayback?expire=1610649785&ei=WDwAYLO6OZqlxN8Pj7uZoAE&ip=196.247.175.161&id=o-AL8uuS6Kapv1_nvU6cVqx7o0Lx30vazs3xvrZMpwh_F8&itag=22&source=youtube&requiressl=yes&mh=IO&mm=31%2C26&mn=sn-5hne6nlk%2Csn-5goeen7y&ms=au%2Conr&mv=u&mvi=5&pl=24&vprv=1&mime=video%2Fmp4&ns=1kw59uBmcCCLu2rMM7UvQgIF&ratebypass=yes&dur=1946.923&lmt=1604212857446769&mt=1610627967&fvip=5&c=WEB&txp=5535432&n=InsUbjT-zmJh8Q&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl&lsig=AG3C_xAwRAIgMKSDrAw9miOYJ2rkrhfVZRZGt6v9-Pf2_4sDq9k20RICIHoRgPafrHASjDONipwzy21ADpMwAwMNvt-0b3XJJCO2&sig=AOq0QJ8wRQIgIv1WMsRnh2UgWsk9-71SsTOuMeh_YTjyqWV103_s_p4CIQDXE9GsXiUGEGJwMlHLdeRXzp4sMXrOwBN60s3Xdbm9tg==&title=THUY%E1%BA%BET+MINH+%7C+Ng%C3%B4n+T%C3%ACnh+Hoa+Ng%E1%BB%AF+2020+Si%C3%AAu+Hay+%7C+B%E1%BA%A1n+G%C3%A1i+L%E1%BA%A7u+D%C6%B0%E1%BB%9Bi+Xin+H%C3%A3y+K%C3%BD+Nh%E1%BA%ADn+-+T%E1%BA%ADp+02', 'tai', 'Siêu nhanh', '2021-01-14 12:38:38'),
(31, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 3', 'https://redirector.googlevideo.com/videoplayback?expire=1610649808&ei=cDwAYLy_J5KqpATR8brQBg&ip=196.245.182.212&id=o-AOyeb2JxaqcsGMN_4b6A-cMAgYypipBD5cq0SJOi3zL0&itag=22&source=youtube&requiressl=yes&mh=sG&mm=31%2C29&mn=sn-4g5edne6%2Csn-4g5e6ns6&ms=au%2Crdu&mv=m&mvi=2&pl=24&initcwndbps=1858750&vprv=1&mime=video%2Fmp4&ns=BnhNJefFYtRyqTMtQ_UqPzsF&ratebypass=yes&dur=1988.278&lmt=1587205004869670&mt=1610628048&fvip=2&c=WEB&txp=5535432&n=hOzXkR4ucj6l6H&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRQIgbOyFYP3ExwspjnD8iaoSpp-J4CXHUC7nFWF_k_JCj5kCIQCKsKeAVT6dZHCTdVnUkPUp4Ly9zrXBD1y6GA-5t6zX3A%3D%3D&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRgIhAO2MB2TAqu3mNNJixDoG8nX6IPrhb5S06hsqU2-IxW_iAiEA3ccLV6rvUzxJ7e7Cv-mWOHjMCyklR53Wl2WmzHwkeHw%3D&title=THUY%E1%BA%BET+MINH+%7C+Ng%C3%B4n+T%C3%ACnh+Hoa+Ng%E1%BB%AF+2020+Si%C3%AAu+Hay+%7C+B%E1%BA%A1n+G%C3%A1i+L%E1%BA%A7u+D%C6%B0%E1%BB%9Bi+Xin+H%C3%A3y+K%C3%BD+Nh%E1%BA%ADn+-+T%E1%BA%ADp+03', 'tai', 'Siêu nhanh', '2021-01-14 12:38:46'),
(32, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 4', 'https://redirector.googlevideo.com/videoplayback?expire=1610649834&ei=ijwAYKunIp-C6dsPlISl6AI&ip=185.182.186.13&id=o-AIan4AuTHNfgNCtXT-5jovRhYA5tHqm-5OOTIEagzSLH&itag=22&source=youtube&requiressl=yes&mh=yj&mm=31%2C26&mn=sn-5hne6nlk%2Csn-5goeen76&ms=au%2Conr&mv=u&mvi=1&pl=24&vprv=1&mime=video%2Fmp4&ns=AoEzivcZ9VOWl7hAvwWUW4EF&ratebypass=yes&dur=1879.214&lmt=1604009041488731&mt=1610627967&fvip=1&c=WEB&txp=5535432&n=9KVzdV9s45EcUj&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRAIgIQfI-aeDb-REj-75AySma-uhC35vGhSlePi5kUHGtgYCIEDcljP1d4VDsVP2teBlymH6dMuk1oeoISlJPkCsTd8Y&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl&lsig=AG3C_xAwRQIgaDEGFfDAXqYYqbVZh4kN4YDCNFOJaIw7VBdVyeYGbXACIQCF9t_hzTEUo75M9_sNfkMzPmDmYw1GU64DOYZHFkaAtg%3D%3D&title=THUY%E1%BA%BET+MINH+%7C+Ng%C3%B4n+T%C3%ACnh+Hoa+Ng%E1%BB%AF+2020+Si%C3%AAu+Hay+%7C+B%E1%BA%A1n+G%C3%A1i+L%E1%BA%A7u+D%C6%B0%E1%BB%9Bi+Xin+H%C3%A3y+K%C3%BD+Nh%E1%BA%ADn+-+T%E1%BA%ADp+04', 'tai', 'Siêu nhanh', '2021-01-14 12:38:51'),
(33, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 5', 'https://redirector.googlevideo.com/videoplayback?expire=1610649857&ei=oTwAYMTiL4GU2_gPuPuKmAQ&ip=38.145.123.227&id=o-AABRlguNFfqJKBLy6zqvWrMQVq_2T0AGufkL_cZ0RJtf&itag=22&source=youtube&requiressl=yes&mh=So&mm=31%2C26&mn=sn-q4fl6ney%2Csn-vgqsknse&ms=au%2Conr&mv=m&mvi=5&pl=22&initcwndbps=1425000&vprv=1&mime=video%2Fmp4&ns=BMahtGLTy1XO7rmuMuN7eZMF&ratebypass=yes&dur=1928.533&lmt=1604058634538922&mt=1610628048&fvip=5&c=WEB&txp=5535432&n=4U4MqF1YDqS8cy&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRQIgJIYVl5gZG-jSINnraWecVvL11RqOBT2oYBVAYIP63WACIQCLUEa-C8I_WB_kqMtwlsva2FOgo2aEO9zfqwDirgMaIg%3D%3D&sig=AOq0QJ8wRQIhALqyWNKBRqrVTa3amlWeKup5RrjNJn266V5cafSSA1WFAiBg4iH-q6UI3LekrXSgIWugy31Pi-7gRRbG1liiH_XwMw==&title=THUY%E1%BA%BET+MINH+%7C+Ng%C3%B4n+T%C3%ACnh+Hoa+Ng%E1%BB%AF+2020+Si%C3%AAu+Hay+%7C+B%E1%BA%A1n+G%C3%A1i+L%E1%BA%A7u+D%C6%B0%E1%BB%9Bi+Xin+H%C3%A3y+K%C3%BD+Nh%E1%BA%ADn+-+T%E1%BA%ADp+05', 'tai', 'Siêu nhanh', '2021-01-14 12:39:00'),
(34, 'ban-gai-duoi-lau-xin-ky-nhan--2021', 'Tập 6', 'https://redirector.googlevideo.com/videoplayback?expire=1610649888&ei=wDwAYJ_4LaKQ8gPFgL_oBg&ip=196.242.72.93&id=o-AE7ePZR2IJkSts2DtP6EUyrpX4nvcjQD3_pkiyKkne9q&itag=22&source=youtube&requiressl=yes&mh=vI&mm=31%2C26&mn=sn-4g5ednsl%2Csn-5hne6n7e&ms=au%2Conr&mv=u&mvi=2&pl=24&vprv=1&mime=video%2Fmp4&ns=Bbf8j5nmg6jc1eR0Xysm9hgF&ratebypass=yes&dur=2034.811&lmt=1603806408648657&mt=1610627967&fvip=2&c=WEB&txp=5535432&n=9FYnpk734G_iOK&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRAIgAYzeSSP43Cb4oax3okFGhC5qGVR7UegDiO7BnUDzN_oCIA6rPaqbmL9hh3D-IhL9EKSJK9OEdVfydcd_BIaiOufY&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl&lsig=AG3C_xAwRAIgC40GcyKMDfSx8o4w53lSRl1vdcGnHFXcgPHjxdtCr1oCIAjzHh28phbCUYM1CYcMofR7s0PoT3xjGNiBV64j5TGy&title=THUY%E1%BA%BET+MINH+%7C+Ng%C3%B4n+T%C3%ACnh+Hoa+Ng%E1%BB%AF+2020+Si%C3%AAu+Hay+%7C+B%E1%BA%A1n+G%C3%A1i+L%E1%BA%A7u+D%C6%B0%E1%BB%9Bi+Xin+H%C3%A3y+K%C3%BD+Nh%E1%BA%ADn+-+T%E1%BA%ADp+06', 'tai', 'Siêu nhanh', '2021-01-14 12:39:08'),
(37, 'bac-si-xu-la---doctor-stranger-2014---2014-bomtanhd--2014', 'Tập 2', 'https://www.youtube.com/embed/LhiUhCzKZKY', 'xem', 'Siêu nhanh', '2021-01-14 12:51:45'),
(38, 'bac-si-xu-la---doctor-stranger-2014---2014-bomtanhd--2014', 'Tập 3', 'https://www.youtube.com/embed/qNosiJaG5c8', 'xem', 'Siêu nhanh', '2021-01-14 12:52:04'),
(39, 'bac-si-xu-la---doctor-stranger-2014---2014-bomtanhd--2014', 'Tập 4', 'https://www.youtube.com/embed/qNosiJaG5c8', 'xem', 'Siêu nhanh', '2021-01-14 12:52:50'),
(40, 'bac-si-xu-la---doctor-stranger-2014---2014-bomtanhd--2014', 'Tập 5', 'https://www.youtube.com/embed/qNosiJaG5c8', 'xem', 'Siêu nhanh', '2021-01-14 12:52:54'),
(65, 'bac-si-xu-la---doctor-stranger-2014---2014-bomtanhd--2014', 'Full ', 'https://www.fshare.vn/folder/5D8OH3RBWEXA97X?token=1610718285', 'tai', 'Siêu nhanh', '2021-01-15 13:45:22'),
(46, 'ban-dao-peninsula-2020---2020-bomtanhd--2020', 'Full hd', 'https://www.youtube.com/embed/FZNa2NAE2VA', 'xem', 'Siêu nhanh', '2021-01-14 12:57:03'),
(47, 'ban-dao-peninsula-2020---2020-bomtanhd--2020', 'Full hd', 'https://file206.iijj.nl/?file=M3R4SUNiN3JsOHJ6WWQ3aTdPRFA4NW1rRVJIOG1mWXowL012eXh4b0xLUmFyNGhrbk42dkxzdEdLK3dpd3B5ckZKVmQrM1h3ZU42UE9VVEMvYkF2VEhlSXZQSW91ejNLOThWcmNjWjlXZy92bE9tbjNYb2syVldsUVl1YlJhZ1lZR2N2NVVacTN5T3MzUERBc1VpOTRWdTBxRmlYZVNZUDkya3RNT1dFdE0xdWdTeVpTZTYyM3BFUnFHaWM3WmNDbDd1VHNSYkp4YWN5dE9ONFVWSjlkcTlIMXBmMzF2WHpxRWdQaVp3ajAwT2JvZXVvQXBjS0ZLeVFmVDVQT2pjTDYrcnJZeE1Sd3lrSDcyT3g4cEIzdmpKWlRvWlB4bVBtMTlQTU5BbTVRWkdnSDRIS0ovanA0TUw4N2FSWGtRZkV2dlBPanA5VW53V2lGWm1wRjRGZTRoMTE4dlhGOXRwcjBVRG9pZz09', 'tai', 'Siêu nhanh', '2021-01-14 12:57:23'),
(48, 'vat-the-bi-nguyen-rua---sorcery-fight-2020---2020-bomtanhd--2020', 'Tập 2', 'https://www.youtube.com/embed/cjBt8nk_KkI', 'xem', 'Siêu nhanh', '2021-01-14 13:02:58'),
(49, 'vat-the-bi-nguyen-rua---sorcery-fight-2020---2020-bomtanhd--2020', 'Tập 3', 'https://www.youtube.com/embed/cjBt8nk_KkI', 'xem', 'Siêu nhanh', '2021-01-14 13:03:01'),
(50, 'vat-the-bi-nguyen-rua---sorcery-fight-2020---2020-bomtanhd--2020', 'Tập 2', 'https://drive.google.com/file/d/134DHJG0uR9hyf9gunAwWGpdyA35GWUcK/view?usp=sharing', 'tai', 'Siêu nhanh', '2021-01-14 13:03:08'),
(51, 'vat-the-bi-nguyen-rua---sorcery-fight-2020---2020-bomtanhd--2020', 'Tập 3', 'https://drive.google.com/file/d/134DHJG0uR9hyf9gunAwWGpdyA35GWUcK/view?usp=sharing', 'tai', 'Siêu nhanh', '2021-01-14 13:03:11'),
(52, 'nguoi-tien-phong-2020---2020-bomtanhd--2020', 'Full hd', 'https://www.youtube.com/embed/FJl2SCFy3kA', 'xem', 'Siêu nhanh', '2021-01-14 13:04:51'),
(53, 'nguoi-tien-phong-2020---2020-bomtanhd--2020', 'Full hd', 'https://file201.iijj.nl/?file=M3R4SUNiN3JsOHJ6WWQ3aTdPRFA4NW1rRVJHSHYvb3AwK0V5Z2pOd1JJb2NxSVZrKyt1dUpJSm5hNFJjaUt2eUdKVjhzQjJPTlBMYmRTclk4N29FQnlxajlKMFEvQnFldHF3Q0hKQlRYMTNXazZPR3gzUlVxRUxVSzh5R0liQkRkREplOGdWQjhtT3g2dVdmbEJ6MnV6Yi85eC9USjI5Qm1TNEZPcjcyNU5SeDJtN0tKTWZzMnAxT2hXTzhzZDlvaXY2Vi8wVFZsL2RuNkpsUUR3RldVZFVFZ0pUeWxNYUpoaFJjb3N4WDdrV3g3TWYzUTd3VVdZUzZiWHdpYm5CWjFyaTlEQXRRd2pWS3ZtcTMrcW9hL21FZGVmMHd1a1N0OXVmOWN6MmRPNURRUW9IUWI3cWI4b0NyeE9BenNrRFZvNjNFazVjTDIwTzNRTUh5ZWNVQXNFRXJucStNNjhzSnlsZXc1UWNQamVNMnlVVDFTRUkxVDROME4zOGFQc2tET0NNUnBjWGR1L1p1cmFCSGUwTGszTG9rU09NeXFwLzAwYklTeWhHSzhkRXlEd2pLNzNhelNvREJTRzFxc3lKc0NHaUhSbkppOVFZbFVHSEhtOFI4N0YzK1hOazlhekpkMUhXNVJ3bmZPcGtwZHJxUWtzT0pmQ1Ridm9vR3g0UXhCaHRkbk95TzlRTkxqTWFPOGg4PQ%3D%3D', 'tai', 'Siêu nhanh', '2021-01-14 13:07:30'),
(54, 'phu-thuy-hoc-duong-2020---the-craft-legacy-2020-bomtanhd-the-craft-legacy-2020', 'Full hd', 'https://www.youtube.com/embed/cb_EgpY8R3Q', 'xem', 'Siêu nhanh', '2021-01-14 13:09:08'),
(55, 'phu-thuy-hoc-duong-2020---the-craft-legacy-2020-bomtanhd-the-craft-legacy-2020', 'Full hd', 'https://drive.google.com/file/d/134DHJG0uR9hyf9gunAwWGpdyA35GWUcK/view?usp=sharing', 'tai', 'Siêu nhanh', '2021-01-14 13:09:30'),
(63, 'son-tung-m-tp-sky-tour-movie-2020---sky-tour-the-movie-2020-bomtanhd-sky-tour-the-movie-2020', 'Full hd', 'https://drive.google.com/file/d/134DHJG0uR9hyf9gunAwWGpdyA35GWUcK/view?usp=sharing', 'tai', 'Siêu nhanh', '2021-01-15 13:42:17'),
(56, 'seal-team-2017---seal-team-2017-bomtanhd-seal-team-2017', 'Full hd', 'https://www.youtube.com/embed/NnCA-0RJpZs', 'xem', 'Siêu nhanh', '2021-01-14 13:11:01'),
(57, 'seal-team-2017---seal-team-2017-bomtanhd-seal-team-2017', 'Full hd', 'https://drive.google.com/file/d/134DHJG0uR9hyf9gunAwWGpdyA35GWUcK/view?usp=sharing', 'tai', 'Siêu nhanh', '2021-01-14 13:13:42'),
(64, 'vat-the-bi-nguyen-rua---sorcery-fight-2020---2020-bomtanhd--2020', 'Tập 1', 'https://drive.google.com/file/d/134DHJG0uR9hyf9gunAwWGpdyA35GWUcK/view?usp=sharing', 'tai', 'Siêu nhanh', '2021-01-15 13:43:19'),
(58, 'hoa-moc-lan-2020-mulan-2020', 'Full hd', 'https://www.youtube.com/embed/Qh8DAwHk7Bg', 'xem', 'Siêu nhanh', '2021-01-14 13:21:24'),
(59, 'hoa-moc-lan-2020-mulan-2020', 'Full hd', 'https://file39.iijj.nl/?file=M3R4SUNiN3JsOHJ6WWQ3aTdPRFA4NW1rRVJIOG11WW9tZHR4bFVCeFVlQnRwWmttMk9yckRzWlpKNkljaHAyckJkWWYwRERmZmRtWU9FUzZvNW95UzJlTjh1bHE2Mi9mdllNd0RZOTNYeHI5cFBhaWdqa3J6RlhUWjlmWUFhMVVhRElwaFUxemgyUEI2dktSNnpQaDRYQ3NzMGpOYnlzSCtYWkFNT0RjOEpGM2h6bVpQOGpsMElRTW9paWc3NVZmMC9XT2l4Yi8xN29CdmRKeVVrMW5lNjlPekpqaDF1VEFvSGdvaU1FNCsxcU1vcldFQWFGaVR2V09OenBnYW1RTzdPQ3lhalZZeWowTzhYNjc0cVlvNnoxWUovRWx2akhncUtLK0lDeU9JOUhnR0E9PQ%3D%3D', 'tai', 'Siêu nhanh', '2021-01-14 13:21:54'),
(62, 'chi-chi-em-em-2019---chi-chi-em-em-2019-bomtanhd-chi-chi-em-em-2019', 'Full hd', 'https://drive.google.com/file/d/134DHJG0uR9hyf9gunAwWGpdyA35GWUcK/view?usp=sharing', 'tai', 'Siêu nhanh', '2021-01-15 13:39:56');

-- --------------------------------------------------------

--
-- Table structure for table `nam`
--

CREATE TABLE `nam` (
  `nam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nam`
--

INSERT INTO `nam` (`nam`) VALUES
(1998),
(2005),
(2018),
(2019);

-- --------------------------------------------------------

--
-- Table structure for table `phim`
--

CREATE TABLE `phim` (
  `id` int(11) NOT NULL,
  `tenPhim` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kieu` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'phimle',
  `nam` int(11) NOT NULL,
  `congtysx` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tengoc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ngaydang` timestamp NOT NULL DEFAULT current_timestamp(),
  `theloai` text COLLATE utf8_unicode_ci NOT NULL,
  `quocgia` text COLLATE utf8_unicode_ci NOT NULL,
  `daodien` text COLLATE utf8_unicode_ci NOT NULL,
  `dienvien` text COLLATE utf8_unicode_ci NOT NULL,
  `luotxem` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `phim`
--

INSERT INTO `phim` (`id`, `tenPhim`, `kieu`, `nam`, `congtysx`, `tengoc`, `slug`, `ngaydang`, `theloai`, `quocgia`, `daodien`, `dienvien`, `luotxem`) VALUES
(259, 'Chị Chị Em Em (2019) - Chị Chị Em Em (2019) | BomTanHD', 'phimle', 2019, 'Đang cập nhật', 'Chị Chị Em Em (2019)', 'chi-chi-em-em-2019---chi-chi-em-em-2019-bomtanhd-chi-chi-em-em-2019', '2021-01-13 02:26:29', 'ngon-tinh,hai-huoc,tam-ly,', 'viet-nam,', '', 'tu-hao,', 153),
(253, 'Bạn gái dưới lầu xin ký nhận', 'phimbo', 2020, 'Đang cập nhật', '楼下女友请签收 (2021)', 'ban-gai-duoi-lau-xin-ky-nhan--2021', '2021-01-10 06:09:44', 'ngon-tinh,', 'trung-quoc,', 'vuong-tinh,', 'tu-hao,thanh-long,', 321),
(254, 'Người Tiên Phong (2020) - 急先锋 (2020) | BomTanHD', 'phimle', 2020, 'China Film (Shanghai) International Media Co., China Film Group Corporation, Epitome Capital, Shanghai Lix Entertainment, Shanghai Tencent Pictures Culture Media, Shenzhen Media Film & Television, Tencent Pictures, ', '急先锋 (2020)', 'nguoi-tien-phong-2020---2020-bomtanhd--2020', '2021-01-10 06:12:16', 'vo-thuat,hanh-dong,hinh-su,', 'trung-quoc,hong-kong,', 'vuong-tinh,', 'thanh-long,', 43),
(255, 'Bán Đảo Peninsula (2020) - 반도 (2020) | BomTanHD', 'phimle', 2020, 'Next Entertainment World, RedPeter Film, Vantage Holdings, ', '반도 (2020)', 'ban-dao-peninsula-2020---2020-bomtanhd--2020', '2021-01-10 06:13:09', 'ngon-tinh,hanh-dong,tam-ly,', 'han-quoc,', 'vuong-tinh,', 'tu-hao,', 113),
(256, 'Phù Thủy Học Đường (2020) - The Craft: Legacy (2020) | BomTanHD', 'phimle', 2020, 'Blumhouse Productions, Red Wagon Entertainment, Columbia Pictures, ', 'The Craft: Legacy (2020)', 'phu-thuy-hoc-duong-2020---the-craft-legacy-2020-bomtanhd-the-craft-legacy-2020', '2021-01-10 06:14:12', 'hai-huoc,kinh-di,tam-ly,', 'my,', 'vuong-tinh,', 'tu-hao,', 11),
(260, 'Bác Sĩ Xứ Lạ -  Doctor Stranger (2014) - 닥터 이방인 (2014) | BomTanHD', 'phimbo', 2014, 'Say On Media, ', '닥터 이방인 (2014)', 'bac-si-xu-la---doctor-stranger-2014---2014-bomtanhd--2014', '2021-01-13 04:44:28', 'ngon-tinh,tam-ly,', 'han-quoc,', '', 'thanh-long,', 83),
(261, 'Sơn Tùng M-TP: Sky Tour Movie (2020) - Sky Tour: The Movie (2020) | BomTanHD', 'phimle', 2020, 'Đang cập nhật', 'Sky Tour: The Movie (2020)', 'son-tung-m-tp-sky-tour-movie-2020---sky-tour-the-movie-2020-bomtanhd-sky-tour-the-movie-2020', '2021-01-13 06:34:02', 'hai-huoc,tam-ly,', 'viet-nam,', '', 'thanh-long,', 110),
(262, 'SEAL Team (2017) - SEAL Team (2017) | BomTanHD', 'phimbo', 2017, 'CBS Television Studios, ', 'SEAL Team (2017)', 'seal-team-2017---seal-team-2017-bomtanhd-seal-team-2017', '2021-01-13 08:09:42', 'hanh-dong,hinh-su,', 'my,', '', 'thanh-long,', 107),
(263, 'Vật Thể Bị Nguyền Rủa - Sorcery Fight (2020) - 呪術廻戦 (2020) | BomTanHD', 'phimbo', 2020, 'MAPPA, ', '呪術廻戦 (2020)', 'vat-the-bi-nguyen-rua---sorcery-fight-2020---2020-bomtanhd--2020', '2021-01-14 09:37:02', 'hai-huoc,anime,', 'nhat-ban,', 'shigeru-chiba,', 'asami-seto,junya-enoki,', 12),
(264, 'Hoa Mộc Lan (2020)', 'phimle', 2020, 'Đang cập nhập', 'Mulan (2020)', 'hoa-moc-lan-2020-mulan-2020', '2021-01-14 13:20:31', 'tien-hiep,vo-thuat,', 'trung-quoc,my,', 'shigeru-chiba,', 'asami-seto,', 13);

-- --------------------------------------------------------

--
-- Table structure for table `quocgia`
--

CREATE TABLE `quocgia` (
  `id` int(11) NOT NULL,
  `ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quocgia`
--

INSERT INTO `quocgia` (`id`, `ten`, `slug`) VALUES
(50, 'Việt Nam', 'viet-nam'),
(53, 'Trung Quốc', 'trung-quoc'),
(54, 'Hàn Quốc', 'han-quoc'),
(55, 'Nhật Bản', 'nhat-ban'),
(56, 'Hồng Kông', 'hong-kong'),
(58, 'Âu Mỹ', 'my'),
(59, 'Nga', 'nga');

-- --------------------------------------------------------

--
-- Table structure for table `taikhoan`
--

CREATE TABLE `taikhoan` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `trangthai` int(11) NOT NULL DEFAULT 0,
  `avatar` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gioithieu` text COLLATE utf8_unicode_ci NOT NULL,
  `tuphim` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `danhsachphat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quyen` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `taikhoan`
--

INSERT INTO `taikhoan` (`id`, `username`, `password`, `ten`, `trangthai`, `avatar`, `email`, `gioithieu`, `tuphim`, `danhsachphat`, `quyen`) VALUES
(22, 'nguyenvanquocvuong', '$2y$11$rb7uAQE0E3ouTVl6TF5RBu4fFrQfIErdOyUPt86P.aQYznRBont4q', 'Nguyễn Văn Quốc Vương', 1, '/bomtanhd/public/img/upload/14012021101851122699374_392683805430344_2993264487821284112_n.jpg', 'thamnguyen1500@gmail.com', 'Xin chào đại gia đình BomTanHD...', '253,263,', '', 1),
(24, 'vuong', '$2y$11$S1UnDMVtjs1ZaZEVSVu6q.PvjoDQ0Lezp.9UbRp9x2RHgZ32Ijnv6', 'vuong', 1, 'https://i.pinimg.com/originals/72/eb/b6/72ebb6e15a76ac319e7275d8cb2d6626.jpg', 'ntlv2006@gmail.com', 'Xin chào đại gia đình BomTanHD...', '255,254,', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `theloai`
--

CREATE TABLE `theloai` (
  `maTL` int(11) NOT NULL,
  `tenTL` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `theloai`
--

INSERT INTO `theloai` (`maTL`, `tenTL`, `slug`) VALUES
(25, 'Ngôn Tình', 'ngon-tinh'),
(24, 'Tiên Hiệp', 'tien-hiep'),
(23, 'Hài Hước', 'hai-huoc'),
(22, 'Võ Thuật', 'vo-thuat'),
(26, 'Anime', 'anime'),
(27, 'Hành Động', 'hanh-dong'),
(28, 'Hình Sự', 'hinh-su'),
(29, 'Cương Thi', 'cuong-thi'),
(30, 'Kinh Dị', 'kinh-di'),
(31, 'Tâm Lý', 'tam-ly'),
(32, 'Xã Hội Đen', 'xa-hoi-den'),
(34, 'Viễn Tưởng', 'vien-tuong');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actor`
--
ALTER TABLE `actor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `binhluan`
--
ALTER TABLE `binhluan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chitietbinhluan`
--
ALTER TABLE `chitietbinhluan`
  ADD PRIMARY KEY (`idBinhLuan`);

--
-- Indexes for table `chitietphim`
--
ALTER TABLE `chitietphim`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `danhsachphat`
--
ALTER TABLE `danhsachphat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linkphim`
--
ALTER TABLE `linkphim`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `nam`
--
ALTER TABLE `nam`
  ADD PRIMARY KEY (`nam`);

--
-- Indexes for table `phim`
--
ALTER TABLE `phim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quocgia`
--
ALTER TABLE `quocgia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taikhoan`
--
ALTER TABLE `taikhoan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theloai`
--
ALTER TABLE `theloai`
  ADD PRIMARY KEY (`maTL`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actor`
--
ALTER TABLE `actor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `chitietphim`
--
ALTER TABLE `chitietphim`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT for table `danhsachphat`
--
ALTER TABLE `danhsachphat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `linkphim`
--
ALTER TABLE `linkphim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `phim`
--
ALTER TABLE `phim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT for table `quocgia`
--
ALTER TABLE `quocgia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `taikhoan`
--
ALTER TABLE `taikhoan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `theloai`
--
ALTER TABLE `theloai`
  MODIFY `maTL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
